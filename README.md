# Doelstelling

De huidige iStandaarden leveren koppelvlakspecificaties voor XML-berichten via XSD's (XML Schema Definitions). Momenteel worden deze XDS's genenereerd met behulp van de huidige  `legacy tooling` (*BizzDesign*). 

Deze `legacy tooling` is vanwege gebrekkige ondersteuning aan vervanging toe. Deze verkenning heeft als doel de mogelijkheden tot vervanging daarvan te onderzoeken. Dit op basis van beschikbare componenten uit het `open ecosysteem`.

# Aanpak

De aanpak is gebaseerd op de volgende uitgangspunten:

- Ontkoppel de `data` van `tools` door open data-formaten (YAML, JSON. TOML, XML) te gebruiken
- Gebruik `tools, raamwerken en/of componenten` die binnen een CI/CI-omgeving (GitLab, ...etc.) kunnen worden toegepast, gebruik maken van open data-formaten en worden ondersteund door een (groeiende) omvangrijke internationale community

De basis van de verkenning is de **data** uit een eerdere uitgevoerde verkenning. Zie [iWmo FAIR Demo / Conversie](https://gitlab.com/istddevops/exploration/fair-publiceren/iwmo-fair-demo/-/blob/dev/python/README.md).

Het [HUGO-raamwerk](https://gohugo.io) biedt *functionaliteit* om:
- Verschillende [HUGO / Output Formats](https://gohugo.io/templates/output-formats) (HTML, XML, JSON, ...etc.) te publiceren.
- De afbeelding van *data* naar verschillende *publicatie-formaten* via [HUGO / templates](https://gohugo.io/templates) vorm te geven.

> Deze *functionaliteit* wordt in deze verkenning toegepast om `XSD's` vanuit de `data` te genereren.

## Implementatie

Onderstaande tabel geeft een overzicht welke *HUGO-raamwerk* instellingen worden gebruikt.

| Instellingen | Configuratie-bestand | 
|:-|:-|
| Basis-instellingen | config.yml |
| Definitie van XML-media met XSD-extensie | mediaTypes,yml | 
| Definitie van XSD-data output-formaat | outputFormats.yml | 
| Definitie van *iStandaard-specifieke* instellingen voor URI en NameSpace | params.yml | 
| Koppelingen naar externe *data* (berichten, ...) | module.yml | 

Onderstaande MindMap geeft de structuur van de *HUGO-templates* weer.

```plantuml
@startmindmap
* **layouts**
** **_default**
***_ baseof.xsd
** **partials**
*** docs
**** inject 
*****_ menu-after.html
*** **xsds**
****_ list-xsds.html
****_ xml-header.xsd
****_ schema-header.xsd
****_ model-header.xsd
****_ model-footer.xsd
****_ schema-footer.xsd
****_ build-namespace.xsd
** **xsds**
***_ list.html
***_ single.xsd
@endmindmap
```
